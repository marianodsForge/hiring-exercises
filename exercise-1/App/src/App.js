import React from "react";
import MainCard from "./components/MainCard";
import { ApolloProvider } from "@apollo/client";
import { ApolloClient, InMemoryCache } from "@apollo/client";
import "./styles/index.css";

const client = new ApolloClient({
  uri: "https://graphqlzero.almansi.me/api",
  cache: new InMemoryCache(),
});

function App() {
  return (
    <div className=" background-img font-sans antialiased text-gray-900 leading-normal tracking-wider bg-cover p12">
      <ApolloProvider client={client}>
        <MainCard />
      </ApolloProvider>
    </div>
  );
}

export default App;
