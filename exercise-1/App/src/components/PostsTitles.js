import React from "react";

const PostsTitles = ({ title }) => {
  return (
    <div>
      <p className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">
        Title: {title}
      </p>
    </div>
  );
};

export default PostsTitles;
