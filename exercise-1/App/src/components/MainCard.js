import React from "react";
import { useQuery, gql } from "@apollo/client";
import Loading from "./Loading";
import PostsCard from "./PostsCard";
import ErrorComponent from "./ErrorComponent";
import CardLayout from "./layout/CardLayout";
import MainCardInfo from "./MainCardInfo";

const GET_USER = gql`
  {
    user(id: 1) {
      id
      name
      address {
        city
        street
        suite
        zipcode
        suite
      }
      email
      phone
      company {
        name
      }
    }
  }
`;

const MainCard = () => {
  const { loading, error, data } = useQuery(GET_USER);
  if (loading) return <Loading />;
  if (error) return <ErrorComponent error={error} />;

  const { user } = data;
  return (
    <CardLayout>
      <>
        <div
          id="profile"
          className="w-full lg:w-3/5 z-0 rounded-lg lg:rounded-l-lg lg:rounded-r-none shadow-2xl bg-white opacity-75 mx-6 lg:mx-0"
        >
          <MainCardInfo user={user} />
        </div>
        <PostsCard name={user.name} />
      </>
    </CardLayout>
  );
};

export default MainCard;
