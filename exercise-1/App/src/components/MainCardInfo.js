import React from "react";

const MainCardInfo = ({ user }) => {
  return (
    <div>
      <div className="p-4 md:p-12 text-center lg:text-left">
        <h1 className="text-3xl font-bold pt-8 lg:pt-0">{user.name}</h1>
        <div className="mx-auto lg:mx-0 w-4/5 pt-3 border-b-2 border-teal-500 opacity-25"></div>
        <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start">
          Adress:
        </p>
        <p className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">
          {user.address.street} - {user.address.suite} -{user.address.zipcode} -{" "}
          {user.address.city}
        </p>
        <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start mt-4">
          Email:
        </p>
        <p className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">
          {user.email}
        </p>
        <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start mt-4">
          Phone:
        </p>
        <p className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">
          {user.phone}
        </p>
        <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start mt-4">
          Company:
        </p>
        <p className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">
          {user.company.name}
        </p>
      </div>
    </div>
  );
};

export default MainCardInfo;
