import React from "react";

const CardLayout = ({ children }) => {
  return (
    <div className="max-w-4xl flex flex-row items-center h-auto lg:h-screen  mx-auto my-32 lg:my-0">
      {children}
    </div>
  );
};

export default CardLayout;
