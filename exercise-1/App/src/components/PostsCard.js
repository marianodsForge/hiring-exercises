import React from "react";
import { useQuery, gql } from "@apollo/client";
import Loading from "./Loading";
import ErrorComponent from "./ErrorComponent";
import CardLayout from "./layout/CardLayout";
import PostsTitles from "./PostsTitles";

const GET_USER = gql`
  {
    user(id: 1) {
      posts {
        data {
          id
          title
        }
      }
    }
  }
`;

const PostsCards = ({ name }) => {
  const { loading, error, data } = useQuery(GET_USER);
  if (loading) return <Loading />;
  if (error) return <ErrorComponent error={error} />;

  //Destructuring the Object for better readability
  const { posts } = data.user;

  return (
    <CardLayout>
      <>
        <div className="w-full lg:w-3/4 mx-6 lg:mx-0 h-screen py-12">
          <div className="w-full rounded-lg lg:rounded-r-lg lg:rounded-l-none shadow-2xl bg-white opacity-75 mx-6 lg:mx-0 px-12 h-full overflow-auto">
            <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start mt-4">
              {name}
            </p>
            {posts.data.map((post, id) => (
              <PostsTitles title={posts.data[id].title} key={id} />
            ))}
          </div>
        </div>
      </>
    </CardLayout>
  );
};
// {
//   products.map((product) => (
//     <Col key={product._id} sm={12} md={6} lg={3}>
//       <Product product={product} />
//     </Col>
//   ));
// }

export default PostsCards;
