const fs = require("fs");
const Stripe = require("stripe");
const STRIPE_TEST_SECRET_KEY =
  "rk_test_51EDa6GIrkMUcWfFwXon9vsJYpTqX2eTqbINAUf4fZC7ivToWv59cAPoHdYhmszwL9ZKWJtUUbaCcHtpjl6rWlXLP00C1dcAoUR";
const stripe = Stripe(STRIPE_TEST_SECRET_KEY);

const handler = async (country) => {
  try {
    let finalCustomers = [];

    /* add code below this line */
    //I am asuming all the validations were made previosuly to receive the country welle parsed.

    //Call of Customer - This can also be done with Axios.
    const customers = JSON.parse(fs.readFileSync("./customers.json", "utf8"));

    //Filter the customers by country
    const filterCountry = (customers) => {
      const filtered = Object.values(customers).filter(
        (customer) => customer.country == country
      );
      return filtered;
    };

    const filteredFinal = filterCountry(customers);

    // Transform Country to ISO
    const customers2 = () => {
      for (let i = 0; i < filteredFinal.length; i++) {
        if (filteredFinal[i].country == "united States") {
          filteredFinal[i].country = "US";
        } else if (filteredFinal[i].country == "Spain") {
          filteredFinal[i].country = "ES";
        } else if (filteredFinal[i].country == "France") {
          filteredFinal[i].country = "FR";
        } else if (filteredFinal[i].country == "Germany") {
          filteredFinal[i].country = "DE";
        }
      }
      return filteredFinal;
    };

    // Transform customers to save into Stripe
    const forStripe = customers2();
    forStripe.forEach((cust) => {
      delete cust.first_name;
      delete cust.last_name;
    });

    // for each customer create a Stripe customer
    for (let i = 0; i < forStripe.length; i++) {
      const customer = await stripe.customers.create({
        address: {
          line1: forStripe[i].address_line_1,
          country: forStripe[i].country,
        },
        email: forStripe[i].email,
      });
      // push into finalCustomers the stripe customers with email, country and id as properties.
      finalCustomers.push({
        email: customer.email,
        customerId: customer.id,
        country: customer.address.country,
      });
    }

    // write finalCustomers array into final-customers.json using fs
    fs.writeFile(
      "final-customers.json",
      JSON.stringify(finalCustomers),
      (err) => {
        if (err) throw err;
        console.log("The file has been saved!");
      }
    );

    /* 
      finalCustomers array should look like:
      finalCustomers = [{
          email: test@test.com
          customerId: 1d833d-12390sa-9asd0a2-asdas,
          country: 'ES'
        },
        {
          email: test@test.com
          customerId: 1d833d-12390sa-9asd0a2-asdas,
          country: 'ES'
        }
      }] 
    */
    /* add code above this line */

    console.log(finalCustomers);
  } catch (e) {
    throw e;
  }
};

handler("Spain");
